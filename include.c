//
// Created by falkr4m on 14.10.2020.
//

#include "include.h"
#include <stdio.h>

int send_data(char* data, unsigned int size) {
    receive_data(data, size);
    return 0;
}

void receive_data(char* data, unsigned int size) {
    packet packet1;
    sscanf(data, "%hd %hd %d %f %d %d %hd",
           &packet1.packet_data1.address,
           &packet1.packet_data1.number_data,
           &packet1.packet_data1.instr,
           &packet1.packet_data1.data1.temp,
           &packet1.packet_data1.data1.humidity,
           &packet1.packet_data1.data1.wind_speed,
           &packet1.hash);
    unsigned short received_hash = packet1.hash;
    unsigned short control_hash = Crc16((unsigned char *)&packet1.packet_data1, (unsigned short)sizeof(packet1.packet_data1));
    printf("\nhash: %d\n", control_hash);
    if (received_hash == control_hash) {
        printf("Данные доставлены в целости:\n");
        printf("Адрес: %d\n"
               "Номер пакета: %d\n",
               packet1.packet_data1.address,
               packet1.packet_data1.number_data);
        printf("Температура: %f\n"
               "Влажность: %d\n"
               "Скорость ветра: %d\n",
               packet1.packet_data1.data1.temp,
               packet1.packet_data1.data1.humidity,
               packet1.packet_data1.data1.wind_speed);
    } else {
        printf("Данные были повреждены в ходе передачи\n");
        printf("Вывод поломанных данных:\n");
        printf("Адрес: %d\n"
               "Номер пакета: %d\n",
               packet1.packet_data1.address,
               packet1.packet_data1.number_data);
        printf("Температура: %f\n"
               "Влажность: %d\n"
               "Скорость ветра: %d\n",
               packet1.packet_data1.data1.temp,
               packet1.packet_data1.data1.humidity,
               packet1.packet_data1.data1.wind_speed);
    }
}

unsigned short Crc16(unsigned char *pcBlock, unsigned short len)
{
    unsigned short crc = 0xFFFF;
    unsigned char i;

    while (len--)
    {
        crc ^= *pcBlock++ << 8;

        for (i = 0; i < 8; i++)
            crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
    }
    return crc;
}


void fill_packet(packet* packet, float temp, int humidity, int wind_speed) {
    packet->packet_data1.data1.temp = temp;
    packet->packet_data1.data1.humidity = humidity;
    packet->packet_data1.data1.wind_speed = wind_speed;
    packet->packet_data1.address = 17;
    packet->packet_data1.instr = 0x01;
    packet->packet_data1.number_data = ++number_data;
}