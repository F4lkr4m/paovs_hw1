#ifndef FREERTOS_SOURCE_INCLUDE_DATA_H
#define FREERTOS_SOURCE_INCLUDE_DATA_H

typedef struct define_data
{
    short address;
    short packet_number;
    char command;
    long temperature;
    long humidity;
    long windspeed;
    unsigned short crc16;
} Data_t

typedef struct define_package
{
    Data_t *ptr;
    int ptr_size;
} Package_t;

void DataInit( void );

#endif