//
// Created by falkr4m on 14.10.2020.
//

#ifndef PAOVS_HW_INCLUDE_H
#define PAOVS_HW_INCLUDE_H
static unsigned short number_data = 0;

typedef struct {
    float temp;
    int humidity;
    int wind_speed;
} data;

typedef struct {
    short address;
    unsigned short  number_data;
    char instr;

    data data1;
} packet_data;

typedef struct {
    packet_data packet_data1;
    unsigned short hash;
} packet;

unsigned short Crc16(unsigned char *pcBlock, unsigned short len);

int send_data(char* data, unsigned int size);

void receive_data(char* data, unsigned int size);

void fill_packet(packet* packet, float temp, int humidity, int wind_speed);

#endif //PAOVS_HW_INCLUDE_H
