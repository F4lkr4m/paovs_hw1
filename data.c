#include <stdio.h>
#include <stdlib.h>
#include "data.h"
#include "FreeRTOS.h" 
#include "tesk.h" 
#include "semphr.h"
#include "queue.h"

const unsigned short Crcl6Table[256] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, ОхбОСб, 0x70E7, 0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, OxDlAD, OxElCE, OxFIEF, 0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6, 0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE, 0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485, 0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D, 0x3653, 0x2672, 0x1611, 0x0630, 0x76D7,0x66F6, 0x5695, 0x46B4, 0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC, 0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823, 0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B, 0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0xlA71, 0x0A50, ОхЗАЗЗ, 0x2A12, OxDBFD, OxCBDC, OxFBBF, 0xEB9E, 0x9B79, 0x8B58, ОхВВЗВ, OxABlA, ОхбСАб, 0x7C87, Ox4CE4, 0x5CC5, 0x2C22, ОхЗСОЗ, ОхОСбО, 0xlC41, OxEDAE, 0xFD8F, OxCDEC, OxDDCD, 0xAD2A, OxBDOB, 0x8D68, 0x9D49, 0x7E97, ОхбЕВб, 0x5ED5,Ox4EF4, 0x3E13, 0x2E32, OxlE51, 0x0E70, 0xFF9F, OxEFBE, OxDFDD, OxCFFC, OxBFIB, 0xAF3A, 0x9F59, 0x8F78, 0x9188, 0x81A9, OxBICA, OxAlEB, OxDlOC, 0xC12D, OxF14E, 0xE16F, 0x1080, OxOOAl, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067, 0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E, 0x02Bl, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256, 0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D, 0x34E2, 0x24C3, 0xl4A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C, 0x26D3, 0x36F2, 0x0691, 0xl6B0, 0x6657, 0x7676, 0x4615, 0x5634, 0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB, 0x5844, 0x4865, 0x7806, 0x6827, 0xl8C0, 0x08El, 0x3882, 0x28A3, 0xCB7D, 0xDB5C, 0xEB3F, OxFBlE, 0x8BF9, 0x9BD8, OxABBB, 0xBB9A, 0x4A75, 0x5A54, 0x6A37, 0x7A16, OxOAFl, OxlADO, 0x2AB3, 0x3A92, OxFD2E, OxEDOF, 0xDD6C, OxCD4D, OxBDAA, 0xAD8B, 0x9DE8, 0x8DC9, 0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, OxlCEO, OxOCCl, OxEFlF, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, OxBFBA, 0x8FD9, 0x9FF8, 0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, OxOEDl, OxlEFO};

QueueHandle_t queue;
SemaphoreHandle_t sem_data, semjo, sem_bin;
Data_t receiver_buffer;

void Transmitted ( void *nvParameters ); 
void Data_Channel( void *pvParameters ); 
void Received void *pvParameters );
int sendData( void *pvPackage );
void ReceiveData();
unsigned short Crcl6( char *pcBlock, unsigned short len );

void Datalnit()
{
    queue = xQueueCreate( 5, sizeof( char* ) );
    sem_data = xSemaphoreCreateMutex();
    semjo = xSemaphoreCreateMutex();
    sem_bin = xSemaphoreCreateBinary();
    if( queue && sem_data && semjo )
    {
        if( xTaskCreate( Transmitter, "Transmitter", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+l, NULL ) != pdPASS )
        {
            printf( "Error, couldn't create task Transmitter\r\n" );
            return;
        }

        if( xTaskCreate( Data_Channel, "Data Channel", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, NULL ) != pdPASS )
        {
            printf( "Error, couldn't create task Data Channel\r\n" );
            return;
        }
        if( xTaskCreate( Receiver, "Receiver", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+l, NULL ) != pdPASS )
        {
            printf( "Error, couldn't create task Receiver\r\n" );
            return;
        }
    }
}

void Transmitter( void *pvParameters)
{
    ( void ) pvParameters;
    Package_t package;
    int count = 0;

    for(;;)
    {
        xSemaphoreTake (sem_io, portMAX_DELAY);

        count++;

        if ((package.ptr = malloc(sizeof(Data_t))) == NULL)
        {
            printf("Error, memory allocation fault\n");

            xSemaphoreGive( sem_io);
            
            continue;
        }

        package.ptr->address = 5;
        package.ptr->packet_number = count;
        package.ptr->command = 0x01;

        printf("sending packet # %d\n", package.ptr->packet_number);

        printf("temperature: " );
        fflush(stdin);
        if (scanf("%ld", &(package.ptr->temperature)) == 0)
        {
            printf("error incorrect input\n");

            xSemaphoreGive( sem_io );
            continue;
        }

        printf("Humidity: ");
        fflush(stdin);
        if (scanf( "%lf", &(package.ptr->humidity)) == 0)
        {
            printf("error incorrect input\n");

            xSemaphoreGive( sem_io);
            continue;
        }

        printf("Windspeed: ");
        fflush(stdin);
        if (scanf( "%lf", &(package.ptr->windspeed)) == 0)
        {
            printf("error incorrect input\n");

            xSemaphoreGive( sem_io);
            continue;
        }

        package.ptr->crc16 = Crc16((char*) package.ptr, 17);

        if( SendData ((void *) (&package)))
        {
            printf("error, failed to add package in q\n");
        }

        xSemaphoreGive( sem_io );

        vTaskDelay(100);
    }
}

int SendData (void *pvPackage )
{
    if (xQueueSend( queue, (void*) pvPackage, 0) == pdFALSE)
        return 1;
    else
        return 0;
}

void Data_Channel( void *pvParameters )
{
    ( void ) pvparameters;
    Packaged package; 
    int count = 1;

    for (;;)
    {
        if ( xQueueReceive (queue, (void*) &package, portMAX_DELAY) == pdTRUE)
        {
            xSemaphoreTake( sem_data, portMAX_DELAY);
            if (count % 5 == 0)
            {
                *((char *) package.ptr + 5) += 1;
                count == 0;
            }
            count++;
            receiver_buffer = *(package.ptr);
            free( package.ptr);
            xSemaphoreGive ( sem_data);
            xSemaphoreGive (sem_bin);
        }
    }
}

void Receiver (void *pvParameters)
{
    (void) pvParameters;
    unsigned short crc16_rcv;

    for (;;)
    {
        ReceiveData();

        xSemaphoreTake( sem_io, portMAX_DELAY);

        crc16_rcv = Crc16( (char *) &receiver_buffer, 17);

        if (crc16_rcv != receiver_buffer.crc16)
            printf("error, control sums dont match\n");
        else 
        {
            printf("Temperature: %ld\n", receiver_buffer.temperature);
            printf("Humidity: %ld\n", receiver_buffer.humidity);
            printf("Windspeed: %ld\n", receiver_buffer.windspeed);
        }

        xSemaphoreGive(sem_data);
        xSemaphoreGive(sem_io);
    }
}

void ReceiveData()
{
    xSemaphoreTake(sem_bin, portMAX_DELAY);
    xSemaphoreTake(sem_data, portMAX_DELAY);
}

unsigned short Crc16(char *pcBlock, unsigned short len)
{
    unsigned short crc = 0xFFFF;

    while(len--)
        crc= (crc<<8) ^ Crcl6Table[ (crc>>8) ^ pcBlock++];
    
    return crc;
}
