/*
 * Вариант №5
 * С удаленной метеостанции передаются значения температуры, влажности, скорости ветра в целочисленном виде,
 * кратные 0,1(градусов), 1% и 1 м/с соответственно. Произвести проверку значений и вывести пользователю значения
 * величин
 */

#include <stdio.h>
#include <stdlib.h>
#include "include.h"

char* packet_in_str(packet* packet) {
    char* str = calloc(sizeof(*packet) + 10, sizeof(char));
    if (str == NULL) {
        return -1;
    }

    sprintf(str, "%hd %hd %d %f %d %d %hd" ,
            packet->packet_data1.address,
            packet->packet_data1.number_data,
            packet->packet_data1.instr,
            packet->packet_data1.data1.temp,
            packet->packet_data1.data1.humidity,
            packet->packet_data1.data1.wind_speed,
            packet->hash);
    return str;
}

int main() {
    packet packet1;
    fill_packet(&packet1, (float)10.1, 100, 3);
    packet1.hash = Crc16((unsigned char *) &packet1.packet_data1, (unsigned short)sizeof(packet1.packet_data1));
    printf("%d", packet1.hash);
    char* data = packet_in_str(&packet1);
    send_data(data, sizeof(data));
    return 0;
}
